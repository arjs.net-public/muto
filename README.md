# README
Author: Alex Schiessl  
Date: 2016/05  
Copyright: Alex Schiessl, arjs.net  
Web: http://arjs.net  
License: Unlicense, http://unlicense.org  
Description:    
A simple musik handling command line tool.
---
Documented commands (type help <topic>):
========================================
analyse  find  load    mix   pause  print  save   stop   
clear    help  locate  next  play   quit   sleep  version   
---
(muto) help analyse   
   
Usage: analyse   
           
(muto) help find   
   
Usage: find (song|artist|genre|album|albumartist|year) <string> [--max=<max>] [--file]   
           
(muto) help load   
   
Usage: load (db|mix <name>) <filepath> [--replace|--join]   
   
Options:   
    --replace           Replace existing entires with found entries   
    --join              Only add new entries   
           
(muto) help mix   
   
Usage: mix <name>   
            [--method=(random|asc|desc)]   
            [--length=<length> --unit=(min|max|title|artist|genre)] [--filter=<filters>]   
           
(muto) help pause   
   
Usage: pause   
           
(muto) help print   
   
Usage: print (db|mix [<name>]|queue|played|current)   
           
(muto) help save   
   
Usage: save (db|mix <name>|played) <filepath> [--overwrite] [--m3u]   
   
Options:   
    --overwrite     Overwrite existing file   
           
(muto) help stop   
   
    Usage: stop   
   
(muto) help clear   
   
Usage: clear (db|mix|queue|played|current)   
           
(muto) help locate   
   
Usage: locate <folder> [--filter=<filter>] [--replace|--join]   
   
Options:   
    --filter=<filter>   List of filters like: *.mp3,*.ogg   
    --replace           Replace existing entires with found entries   
    --join              Only add new entries   
           
(muto) help next   
   
    Usage: next   
               
(muto) help play   
   
Usage: play [(song|artist|album|genre|year|mix) <string>]   
           
(muto) help quit   
Quits out of Interactive Mode.   
(muto) help sleep   
   
Usage: sleep <seconds> [--quit]   
   

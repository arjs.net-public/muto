#!/usr/bin/env python 
import os
from setuptools import setup, find_packages


def read(filename):
    return open(os.path.join(os.path.dirname(__file__), filename)).read()

setup(
    name=read('name.txt'),
    version=read('version.txt'),
    author='Alex Schiessl',
    author_email='alexrjs@gmail.com', 
    url='http://arjs.net',
    packages=find_packages('src'), 
    package_dir={'':'src'}, 
    package_data={
        # If any package contains *.txt files, include them:
        '': ['*.txt'],
        # And include any *.dat files found in the 'data' subdirectory
        # of the 'mypkg' package, also:
        '': ['data/*.*'],
    },
    install_requires=[], 
    description=read('description.txt'),
    long_description=read('README.md'),
    scripts=['src/bin/muto']              
)

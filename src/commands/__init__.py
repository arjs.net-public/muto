from glob import glob
from keyword import iskeyword
from os.path import dirname, join, split, splitext

# some constants
__author__ = 'alexrjs'

# some variables
basedir = dirname(__file__)

__all__ = []
for name in glob(join(basedir, '*.py')):
    module = splitext(split(name)[-1])[0]
    if not module.startswith('_') and not iskeyword(module):
        try:
            __import__(__name__ + '.' + module)
        except Exception as e:
            import logging
            ch = logging.StreamHandler()
            ch.setLevel(logging.ERROR)
            logger = logging.getLogger(__name__)
            # logger.addHandler(ch)
            if hasattr(e, 'message)'):
                logger.error('ERROR: {} - %r'.format(e.message if e.message else str(e)), module)
            elif hasattr(e, 'msg') and hasattr(e, 'lineno'):
                logger.error('ERROR: {} - {} ({}/{})'.format(module, e.msg if e.msg else str(e), e.lineno, e.offset))
            elif hasattr(e, 'msg'):
                logger.error('ERROR: {} - {}'.format(module, e.msg if e.msg else str(e)))
            
            if hasattr(e, 'text'):
                logger.error('ERROR: {}'.format(e.text if e.text else str(e)))

            logger.warning('Warning: Ignoring exception while loading the %r plug-in.', module)
        else:
            __all__.append(module)

__all__.sort()

"""
Command: version
"""
# imports
import os
import common
from common.log import view
from common.utils import get_content, read_text_data

# some constants

__author__ = 'alexrjs'
__version__ = '1.0.0.0'
__package__ = 'commands'

# some variables


def version():
    """Simply returns the current version string"""
    return '{} - Version {}'.format(__name__.upper(), __version__)


def run(args):
    """Main run method; Handles the main load of the combination of parameters;
    :type args: object[]
    """
    if args.debug:
        view('>', args)
        view('>', '{} {} {} {}'.format(__name__, __file__, __package__, __version__))

    print_version()
    return 0


def print_version():
    _name = common.shared.configuration.get('common', 'prg_name').strip()
    _version = common.shared.configuration.get('common', 'prg_version').strip()

    print('{} - {}'.format(get_content(_name), get_content(_version)))

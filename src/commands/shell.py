# -*- coding: utf-8 -*-
"""
Command: shell
"""
# imports
import os
import cmd
import copy
import time
import queue
import random
import hashlib
import datetime
import threading
import eyed3
import eyed3.mp3
import common
from common.log import view
from common.utils import docopt_cmd

# class Event(object):
#     def __init__(self, name, data, auto_fire=True):
#         self.name = name
#         self.data = data
#         if auto_fire:
#             self.fire()
#
#     def fire(self, **attributes):
#         for k, v in attributes.iteritems():
#             setattr(self, k, v)
#
#         for fn in Observable.callbacks:
#             fn(self)
#
#
# class Observable(object):
#     callbacks = []
#
#     def subscribe(self, callback):
#         self.callbacks.append(callback)

# some constants
__author__ = 'alexrjs'
__version__ = '1.0.0.0'
__package__ = 'commands'

# some variables
index_detail = {
    'song': ('songs', 'title'),
    'artist': ('artists', 'artist'),
    'album': ('albums', 'album'),
    'genre': ('genres', 'genre'),
    'year': (None, 'year'),
    'mix': ('genres', 'genre'),
    'albumartist': ('albumartists', 'album artist')
}
_empty_info_db = {
    'stats': {
        'artists': 0,
        'album artists': 0,
        'albums': 0,
        'songs': 0,
        'folders': 0,
        'files': 0,
        'problems': 0
    },
    'folders': [],
    'files': {},
    'albums': {},
    'songs': {},
    'artists': {},
    'album artists': {},
    'genres': {},
    'problems': []
}
key_db = {
    'Abm': {'key': '1A', 'id': 10, 'allowed': ['B', 'Ebm', 'Dbm', 'Fm', 'Bm', 'Dm']},
    'B': {'key': '1B', 'id': 210, 'allowed': ['Abm', 'Gb', 'E', 'Ab', 'D', 'F']},
    'Ebm': {'key': '2A', 'id': 20, 'allowed': ['Gb', 'Bbm', 'Abm', 'Cm', 'Gbm', 'Am']},
    'Gb': {'key': '2B', 'id': 220, 'allowed': ['Ebm', 'Db', 'B', 'Eb', 'A', 'C']},
    'Bbm': {'key': '3A', 'id': 30, 'allowed': ['Db', 'Fm', 'Ebm', 'Gm', 'Dbm', 'Em']},
    'Db': {'key': '3B', 'id': 230, 'allowed': ['Bbm', 'Ab', 'Gb', 'Bb', 'E', 'G']},
    'Fm': {'key': '4A', 'id': 40, 'allowed': ['Ab', 'Cm', 'Bbm', 'Dm', 'Abm', 'Bm']},
    'Ab': {'key': '4B', 'id': 240, 'allowed': ['Fm', 'Eb', 'Db', 'F', 'B', 'D']},
    'Cm': {'key': '5A', 'id': 50, 'allowed': ['Eb', 'Gm', 'Fm', 'Am', 'Ebm', 'Gbm']},
    'Eb': {'key': '5B', 'id': 250, 'allowed': ['Cm', 'Bb', 'Ab', 'C', 'Gb', 'A']},
    'Gm': {'key': '6A', 'id': 60, 'allowed': ['Bb', 'Dm', 'Cm', 'Em', 'Bbm', 'Dbm']},
    'Bb': {'key': '6B', 'id': 260, 'allowed': ['Gm', 'F', 'Eb', 'G', 'Db', 'E']},
    'Dm': {'key': '7A', 'id': 70, 'allowed': ['F', 'Am', 'Gm', 'Bm', 'Fm', 'Abm']},
    'F': {'key': '7B', 'id': 270, 'allowed': ['Dm', 'C', 'Bb', 'D', 'Ab', 'B']},
    'Am': {'key': '8A', 'id': 80, 'allowed': ['C', 'Em', 'Dm', 'Gbm', 'Cm', 'Ebm']},
    'C': {'key': '8B', 'id': 280, 'allowed': ['Am', 'G', 'F', 'A', 'Eb', 'Gb']},
    'Em': {'key': '9A', 'id': 90, 'allowed': ['G', 'Bm', 'Am', 'Dbm', 'Gb', 'Bbm']},
    'G': {'key': '9B', 'id': 290, 'allowed': ['Em', 'D', 'C', 'E', 'Bb', 'Db']},
    'Bm': {'key': '10A', 'id': 100, 'allowed': ['D', 'Gbm', 'Em', 'Abm', 'Dm', 'Fm']},
    'D': {'key': '10B', 'id': 300, 'allowed': ['Bm', 'A', 'G', 'B', 'F', 'Ab']},
    'Gbm': {'key': '11A', 'id': 110, 'allowed': ['A', 'Dbm', 'Bm', 'Ebm', 'Am', 'Cm']},
    'A': {'key': '11B', 'id': 310, 'allowed': ['Gbm', 'E', 'D', 'Gb', 'C', 'Eb']},
    'Dbm': {'key': '12A', 'id': 120, 'allowed': ['E', 'Abm', 'Gbm', 'Bbm', 'Em', 'Gm']},
    'E': {'key': '12B', 'id': 320, 'allowed': ['Dbm', 'B', 'A', 'Db', 'G', 'Bb']},
}
translate_key_db = {
    '1A': 'Abm',
    '1B': 'B',
    '2A': 'Ebm',
    '2B': 'Gb',
    '3A': 'Bbm',
    '3B': 'Db',
    '4A': 'Fm',
    '4B': 'Ab',
    '5A': 'Cm',
    '5B': 'Eb',
    '6A': 'Gm',
    '6B': 'Bb',
    '7A': 'Dm',
    '7B': 'F',
    '8A': 'Am',
    '8B': 'C',
    '9A': 'Em',
    '9B': 'G',
    '10A': 'Bm',
    '10B': 'D',
    '11A': 'Gbm',
    '11B': 'A',
    '12A': 'Dbm',
    '12B': 'E',
}
info_db = copy.deepcopy(_empty_info_db)
mix_db = {}
my_args = None
song_number = 0
q = queue.Queue()
threads = []
played = []
current_played = None
pause_played = None


def version():
    """Simply returns the current version string"""
    return '{} - Version {}'.format(__name__.upper(), __version__)


def run(args):
    """Main run method; Handles the main load of the combination of parameters;
    :type args: object[]
    """
    if args.debug:
        view('>', args)
        view('>', '{} {} {} {}'.format(__name__, __file__, __package__, __version__))

    eyed3.log.setLevel(50)
    global my_args
    my_args = args
    Shell().cmdloop()


def get_song_number():
    global song_number
    song_number += 1
    return '{0:04d}'.format(song_number)


def worker():
    while True:
        song = q.get()
        if song is None:
            global threads
            threads = []
            break
        else:
            global played, current_played
            current_played = song
            play_path = common.configuration.get('common', 'play_path').strip()
            play_cmd = common.configuration.get('common', 'play_command').strip()
            play_args = common.configuration.get('common', 'play_args').strip()
            if len(play_args) > 0:
                play_args = play_args.split(':')
            else:
                play_args = []

            play_args.insert(0, play_cmd)
            play_args.append(song)
            played.append(song)
            Shell.print_song(song)
            os.spawnv(os.P_WAIT, play_path, play_args)
            # os.spawnv(os.P_WAIT, '/usr/bin/afplay', ['afplay', '/Volumes/Iomega HDD/Musik/MP3s/C/Coldplay - Clocks.mp3'])
            q.task_done()


class Shell(cmd.Cmd):
    intro = 'Welcome to a interactive musik tool!' \
            + ' (type help for a list of commands.)'
    prompt = '(muto) '

    def emptyline(self):
        return False

    @staticmethod
    def calculate_stats(db):
        if 'stats' not in db:
            print("Error: DB does not have statistics!")
            return False

        stats = db['stats']
        stats['artists'] = len(info_db['artists']) if 'artists' in info_db else 0
        stats['album artists'] = len(info_db['album artists']) if 'album artists' in info_db else 0
        stats['albums'] = len(info_db['albums']) if 'albums' in info_db else 0
        stats['songs'] = len(db['songs']) if 'songs' in info_db else 0
        stats['folders'] = len(db['folders']) if 'folders' in info_db else 0
        stats['files'] = len(db['files']) if 'files' in info_db else 0
        stats['problems'] = len(db['problems']) if 'problems' in info_db else 0
        return True

    @staticmethod
    def print_stats(db, skip_zero=False):
        if 'stats' not in db:
            print("Error: DB does not have statistics!")
            return False

        labels = {
            'artists': 'Artists',
            'album artists': 'Album Artists',
            'albums': 'Albums',
            'songs': 'Songs',
            'folders': 'Folders',
            'files': 'Files',
            'problems': 'Problems',
        }
        stats = db['stats']
        for label in labels:
            value = stats[label] if label in stats else 0
            if value == 0:
                if not skip_zero:
                    print("Located ", labels[label], ":", value)
            else:
                print(labels[label], ":", value)

        return True

    @staticmethod
    def print_mix(mix, songs):
        count = 0
        print("Mix:", mix)
        for song in songs:
            if song is None:
                continue

            pl = Shell.print_song(song)
            count += pl

        print("Total: %02d:%02d (%ds)" % (count / 60, count % 60, count))
        print("Length: %d Songs" % len(songs))
        print()

    @staticmethod
    def print_song(song):
        pl = Shell.calculate_length([song])
        song_key, song_bpm, song_genre = Shell.gather_info(song)
        print("%02d:%02d (%ds) - %s, %s, %s - %s" % \
              (pl / 60, pl % 60, pl, song_key, song_bpm, song_genre, song))
        return pl

    @staticmethod
    def add_problem_file(info_element, song_file):
        if song_file not in info_db['problems']:
            info_db['problems'].append(song_file)

        print("Warning: Problems with {} in {}".format(info_element, song_file))

    @staticmethod
    def set_info_element(element, elements, info, info_element, song_file):
        if elements is not None:
            _md5 = Shell.get_md5(element)
            if _md5 not in elements:
                elements[_md5] = element
            else:
                if elements[_md5] != element:
                    Shell.add_problem_file(info_element, song_file)

                    # print "Warning: Problems with {} in {}".format(info_element, song_file)
                    print("Warning: Stored '{}' / Found '{}'".format(elements[_md5], element))

            if info_element not in info:
                info[info_element] = _md5
            else:
                if info[info_element] != _md5:
                    Shell.add_problem_file(info_element, song_file)

                    # print "Warning: MD5 Problems with {} in {}".format(info_element, song_file)
                    print("Warning: Stored '{}' / Found '{}'".format(elements[_md5], elements[info[info_element]]))

        else:
            if info_element not in info:
                info[info_element] = element

    @staticmethod
    def get_md5(element):
        if not isinstance(element, unicode):
            _tmp = (unicode(element).strip().upper()).encode('utf-8')
        else:
            _tmp = element.strip().upper().encode('utf-8')

        _md5 = hashlib.md5()
        _md5.update(_tmp)
        return _md5.hexdigest()

    @staticmethod
    def find_helper(arg):
        find = arg['<string>']
        if not find:
            print("Info: Empty search string!")
            return None

        at_begin = find.startswith('^')
        at_end = find.endswith('$')
        if at_begin:
            find = find[1:]
        if at_end:
            find = find[:-1]

        find = find.strip().upper()
        id_filter = [x for x in index_detail.keys() if x in arg and arg[x]]
        if len(id_filter) == 0:
            print("Info: Unknown mode!")
            return None

        index, detail = index_detail[id_filter[0]]
        files = info_db['files']
        if arg['--file']:
            if index is not None:
                if at_begin:
                    result = [k for k in info_db[index]
                              if info_db[index][k].startswith(find) and find in info_db[index][k].upper()]
                elif at_end:
                    result = [k for k in info_db[index]
                              if info_db[index][k].upper().endswith(find) and find in info_db[index][k].upper()]
                else:
                    result = [k for k in info_db[index] if find in info_db[index][k].upper()]

                result = [files[kk].get('filename') for k in result for kk in files
                          if detail in files[kk] and k in files[kk][detail]]
            else:
                result = [files[k].get('filename') for k in files if find in files[k][detail]]

        else:
            if index is not None:
                if at_begin:
                    result = [info_db[index][k] for k in info_db[index]
                              if info_db[index][k].startswith(find) and find in info_db[index][k].upper()]
                elif at_end:
                    result = [info_db[index][k] for k in info_db[index]
                              if info_db[index][k].upper().endswith(find) and find in info_db[index][k].upper()]
                else:
                    result = [info_db[index][k] for k in info_db[index] if find in info_db[index][k].upper()]
            else:
                result = [files[k][detail] for k in files if find in files[k][detail]]

        return result

    @staticmethod
    def option_helper(arg, options, default):
        if arg is None:
            value = default
        else:
            value = [x for x in options if x in arg]
            if len(value) == 0:
                print("Info: Unknown option, using default:", default)
                value = default
            else:
                value = value[0]

        return value

    @staticmethod
    def length_helper(arg):
        default_length = 10
        if arg is None:
            return default_length
        else:
            try:
                length = int(arg)
            except ValueError:
                print("Info: Cannot determine length, using default:", default_length)
                length = default_length
            except TypeError:
                print("Info: Cannot determine length, using default:", default_length)
                length = default_length

        return length

    @staticmethod
    def song_filter_helper(arg):
        filters = {}
        if arg is not None:
            song_filter = arg
            elements = arg.split(';')
            for element in elements:
                # print element
                if ':' not in element:
                    print("Warning: ignored filter '%s'" % element)
                    continue

                k, f = element.split(':')
                # print k, f
                if ',' in f:
                    f1, f2 = f.split(',')
                    if f1.isalpha() and f2.isalpha():
                        filters[k] = (f1, f2)
                    elif f1.isalpha() and f2 in ['=', '+', '-', '?', '!']:
                        filters[k] = (f1, f2)
                    elif not f1.isalpha() and not f2.isalpha():
                        # print "base", f1, "+/-", f2
                        filters[k] = (int(f1) - int(f2), int(f1) + int(f2))
                elif '-' in f:
                    f1, f2 = f.split('-')
                    # print "from", f1, "to", f2
                    filters[k] = (f1, f2)
                else:
                    filters[k] = (f, None)

            # common.pprint(filters)
        else:
            song_filter = '*'

        return song_filter, filters

    @staticmethod
    def result_helper(method, length, unit, filters):
        files = info_db['files']
        fl = len(filters)
        if unit in ['min', 'max']:
            index = 'files'
            detail = 'length'
            corrected_length = length if fl <= 0 else len(info_db[index])
            values = sorted(info_db[index].items(), key=lambda y: y[1][detail])

        elif unit in index_detail:
            index, detail = index_detail[unit]
            if index not in info_db:
                print("Info: Not supported unit!")
                return

            corrected_length = length if fl <= 0 else len(info_db[index])
            values = sorted(info_db[index].items(), key=lambda y: y[1])

        else:
            print("Info: Not supported unit!")
            return

        if 'asc' == method:
            if fl == 0:
                values = values[:corrected_length]

            # l_values = len(values)

        elif 'desc' == method:
            l = len(info_db[index]) - corrected_length
            if l >= 0 and fl == 0:
                values = values[l:]

            # l_values = len(values)
            values = reversed(values)

        else:
            items = []
            stack = []
            while len(stack) < corrected_length:
                r = random.randint(0, len(info_db[index]) - 1)
                if r not in stack:
                    items.append(values[r])
                    stack.append(r)

            values = items
            # l_values = len(values)

        if fl > 0:
            if unit in ['min', 'max']:
                count = 0
                rest = target = 60 * length
                for i, value in enumerate(values):
                    key = value[0]
                    current_length = value[1][detail]
                    if current_length > rest:
                        if 'min' == unit and count <= target:
                            for item in filters:
                                if int(filters[item][0]) <= int(files[key][item]) <= int(filters[item][1]):
                                    yield (current_length, value[1]['filename'])
                                    count += current_length
                                    # rest = target - count
                                else:
                                    continue
                            else:
                                continue

                        else:
                            continue

                    if rest <= 0:
                        return

                    if count + current_length <= target:
                        for item in filters:
                            if int(filters[item][0]) <= int(files[key][item]) <= int(filters[item][1]):
                                yield (current_length, value[1]['filename'])
                                count += current_length
                                # rest = target - count
                            else:
                                continue
                        else:
                            continue

                    elif 'max' == unit and count <= target:
                        tmp = [x for x in files if files[x][detail] <= rest]
                        if len(tmp) == 0:
                            return

                        elif len(tmp) == 1:
                            for item in filters:
                                if int(filters[item][0]) <= int(files[key][item]) <= int(filters[item][1]):
                                    yield (current_length, tmp[1].get('filename'))
                                    count += current_length
                                    return
                                else:
                                    continue
                            else:
                                continue
                    else:
                        return

            else:
                count = 0
                added_list = []
                # print l_values
                next_key = None
                for value in values:
                    if count >= length:
                        break

                    for k in files:
                        if count >= length:
                            break

                        if detail not in files[k]:
                            print("Warning: no field '%s' in song '%s'!" % (detail, files[k]))
                            continue

                        if files[k][detail] != value[0]:
                            continue

                        ok = 0
                        do_check = True
                        for item in filters:
                            if 'bpm' == item:
                                if int(filters[item][0]) <= int(files[k][item]) <= int(filters[item][1]):
                                    ok += 1

                            elif 'key' == item:
                                if next_key is None:
                                    next_key = translate_key_db[filters[item][0]]

                                if files[k][item] == next_key:
                                    ok += 1
                                    # print key_db[next_key]['key']
                                    r = random.randint(0, 5)
                                    next_key = key_db[next_key]['allowed'][r]

                            elif 'text' == item:
                                letter1 = filters[item][0].upper()
                                letter2 = filters[item][1].upper()
                                idx = files[k][detail]
                                tmp = info_db[index][idx].upper()

                                if letter2 is None:
                                    for l in letter1:
                                        if tmp.startswith(l):
                                            ok += 1
                                else:
                                    if '=' == letter2:
                                        do_check = False
                                        if tmp == letter1:
                                            ok += 1
                                        elif detail == 'genre' and letter1 in tmp:
                                            ok += 1
                                    elif '-' == letter2:
                                        for l in letter1:
                                            if not tmp.startswith(l):
                                                ok += 1
                                                break
                                    elif '+' == letter2:
                                        for l in letter1:
                                            if tmp.startswith(l):
                                                ok += 1
                                    elif '?' == letter2:
                                        if letter1 in tmp:
                                            ok += 1
                                    elif '!' == letter2:
                                        if letter1 not in tmp:
                                            ok += 1
                                    elif letter2.isalpha():
                                        for i in xrange(ord(letter1), ord(letter2)):
                                            if tmp.startswith(chr(i)):
                                                ok += 1
                                                break

                            if ok == fl:
                                check = info_db[index][files[k][detail]]
                                if do_check and check in added_list:
                                    continue

                                added_list.append(check)
                                song = files[k].get('filename')
                                yield (value[1], song)
                                count += 1
                                ok = 0
                                # Shell.print_song(song)

        else:
            if unit in ['min', 'max']:
                count = 0
                rest = target = 60 * length
                for i, value in enumerate(values):
                    current_length = value[1][detail]
                    if current_length > rest:
                        if 'min' == unit and count <= target:
                            yield (current_length, value[1]['filename'])
                            count += current_length
                            rest = target - count

                        else:
                            continue

                    if rest <= 0:
                        return

                    if count + current_length <= target:
                        yield (current_length, value[1]['filename'])
                        count += current_length
                        rest = target - count

                    elif 'max' == unit and count <= target:
                        tmp = [x for x in files if files[x][detail] <= rest]
                        if len(tmp) == 0:
                            return

                        elif len(tmp) == 1:
                            yield (current_length, tmp[1].get('filename'))
                            count += current_length
                            return
                    else:
                        return

            else:
                count = 0
                do_check = True
                added_list = []
                for value in values:
                    for k in files:
                        if count >= length:
                            break

                        if files[k][detail] == value[0]:
                            check = info_db[index][files[k][detail]]
                            if do_check and check in added_list:
                                continue

                            added_list.append(check)
                            song = files[k].get('filename')
                            yield (value[1], song)
                            count += 1


    @staticmethod
    def calculate_length(arg):
        if isinstance(arg, list):
            count = 0
            for element in arg:
                _md5 = Shell.get_md5(element)
                if _md5 in info_db['files']:
                    count += int(info_db['files'][_md5].get('length'))
                else:
                    tmp = [x for x in info_db['files'] if info_db['files'][x].get('filename') == element]
                    if len(tmp) == 1:
                        count += int(info_db['files'][tmp[0]].get('length'))
                    else:
                        print("Info: Skipped", element)

            return count
        else:
            return 0

    @staticmethod
    def gather_info(song):
        _md5 = Shell.get_md5(song)
        if _md5 in info_db['files']:
            song_key = info_db['files'][_md5].get('key')
            song_bpm = info_db['files'][_md5].get('bpm')
            genre_id = info_db['files'][_md5].get('genre')
            song_genre = info_db['genres'][genre_id]
        else:
            tmp = [x for x in info_db['files'] if info_db['files'][x].get('filename') == song]
            if len(tmp) == 1:
                song_key = info_db['files'][tmp[0]].get('key')
                song_bpm = info_db['files'][tmp[0]].get('bpm')
                genre_id = info_db['files'][tmp[0]].get('genre')
                song_genre = info_db['genres'][genre_id]
            else:
                print("Info: Skipped", song)
                return None, None, None

        song_genre = song_genre.split(', ')[1]
        return song_key, song_bpm, song_genre[:-1]

    @staticmethod
    def cancel_play_queue():
        while not q.empty():
            q.get_nowait()

        q.put(None)

    @staticmethod
    def cancel_current_play():
        global current_played
        current_played = ''

        global threads
        threads = []

        play_cmd = common.shared.configuration.get('common', 'play_command').strip()
        # tmp = os.popen("ps aux | grep 'play -q' | grep -v grep | awk '{ print $2 }'").read()
        kill_str = "ps aux | grep '" + play_cmd + "' | grep -v grep | awk '{ print $2 }'"
        tmp = os.popen(kill_str).read()
        pids = [x for x in tmp.split('\n') if x]
        for pid in pids:
            # print 'Info: Stopping play with pid', pid
            try:
                os.kill(int(pid), 15)
            except TypeError:
                pass
            except ValueError:
                pass

    @staticmethod
    def do_quit(arg):
        """Quits out of Interactive Mode."""

        if arg and my_args.debug:
            print(arg)

        Shell.cancel_play_queue()
        Shell.cancel_current_play()

        print('Good Bye!')
        exit(0)

    @docopt_cmd
    def do_version(self, arg):
        """
Usage: version
        """

        if arg and my_args.debug:
            print(arg)

        print(version())
        # audiofile = eyed3.load("/home/homeboy/Musik/M/Maxim - Was Für 'Ne Welt.mp3")
        # print audiofile.tag.artist, audiofile.tag.title

    @docopt_cmd
    def do_clear(self, arg):
        """
Usage: clear (db|mix|queue|played|current)
        """

        if arg and my_args.debug:
            print(arg)

        if arg['db']:
            global info_db
            info_db = copy.deepcopy(_empty_info_db)
        elif arg['mix']:
            global mix_db
            mix_db = {}
        elif arg['queue']:
            Shell.cancel_play_queue()
        elif arg['current']:
            Shell.cancel_current_play()
        elif arg['played']:
            global played
            played = []

    @docopt_cmd
    def do_print(self, arg):
        """
Usage: print (db|mix [<name>]|queue|played|current)
        """

        if arg['db']:
            common.pprint(info_db)
            Shell.calculate_stats(info_db)
            Shell.print_stats(info_db)

        elif arg['mix']:
            name = arg['<name>']
            if name and name in mix_db:
                Shell.print_mix(name, mix_db[name])

            else:
                for mix in mix_db:
                    Shell.print_mix(mix, mix_db[mix])

        elif arg['queue']:
            Shell.print_mix('[Queue]', q.queue)

        elif arg['played']:
            Shell.print_mix('[Queue]', played)

        elif arg['current']:
            if current_played is not None:
                Shell.print_song(current_played)

    @docopt_cmd
    def do_sleep(self, arg):
        """
Usage: sleep <seconds> [--quit]
        """

        try:
            sleep_time = int(arg['<seconds>'])
        except ValueError:
            sleep_time = 0

        if sleep_time > 0:
            time.sleep(sleep_time)
            print('Sleep time {}s is over!'.format(sleep_time))
            if arg['--quit']:
                Shell.do_quit(arg)
            else:
                Shell.cancel_play_queue()
                Shell.cancel_current_play()

    @docopt_cmd
    def do_stop(self, arg):
        """
Usage: stop
        """
        if my_args.debug:
            print(arg)

        Shell.cancel_play_queue()
        Shell.cancel_current_play()

    @docopt_cmd
    def do_pause(self, arg):
        """
Usage: pause
        """
        if my_args.debug:
            print(arg)

        global pause_played
        if pause_played is None:
            pause_played = []

        pause_played.append(current_played)
        while not q.empty():
            pause_played.append(q.get_nowait())

        Shell.cancel_play_queue()
        Shell.cancel_current_play()

    @docopt_cmd
    def do_next(self, arg):
        """
    Usage: next
        """
        if my_args.debug:
            print(arg)

        Shell.cancel_current_play()

    @docopt_cmd
    def do_save(self, arg):
        """
Usage: save (db|mix <name>|played) <filepath> [--overwrite] [--m3u]

Options:
    --overwrite     Overwrite existing file
        """

        l = 0
        pl = 0
        file_path = arg['<filepath>']
        if arg['db']:
            if not info_db['files']:
                print("Info: Nothing to save!")
                return

            if arg['--m3u']:
                if not file_path.endswith('.m3u'):
                    file_path = '{}.m3u'.format(file_path)
            else:
                if not file_path.endswith('.db.json'):
                    file_path = '{}.db.json'.format(file_path)

            db = info_db

        elif arg['mix']:
            if arg['<name>'] not in mix_db:
                print("Info: Nothing to save!")
                return

            if arg['--m3u']:
                if not file_path.endswith('.m3u'):
                    file_path = '{}.m3u'.format(file_path)
            else:
                if not file_path.endswith('.mix.json'):
                    file_path = '{}.mix.json'.format(file_path)

            db = {'songs': mix_db[arg['<name>']]}
            l = len(mix_db[arg['<name>']])
            pl = Shell.calculate_length(mix_db[arg['<name>']])

        elif arg['played']:
            if len(played) <= 0:
                print("Info: Nothing to save!")
                return

            if arg['--m3u']:
                if not file_path.endswith('.m3u'):
                    file_path = '{}.m3u'.format(file_path)
            else:
                if not file_path.endswith('.mix.json'):
                    file_path = '{}.mix.json'.format(file_path)

            db = {'played': played}
            l = len(played)
            pl = Shell.calculate_length(mix_db[arg['<filepath>']])

        else:
            print("Info: Nothing to save!")
            return

        db['update'] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        if not common.file_exists(file_path, exception=False):
            if arg['--m3u']:
                songs = []
                if 'files' in db:
                    for key in db['files']:
                        songs.append(db['files'][key]['filename'])
                elif 'songs' in db:
                    songs = db['songs']
                elif 'played' in db:
                    songs = db['played']
                else:
                    print("Info: Nothing to save!")
                    return

                common.write_text_data(file_path, '\n'.join(songs))

            else:
                common.write_json_data(file_path, db)
        else:
            if arg['--overwrite']:
                common.write_json_data(file_path, db)
            else:
                print("Info: File exists!")

        if arg['db']:
            Shell.calculate_stats(info_db)
            Shell.print_stats(info_db)
        else:
            print("Info: Length", l, "Songs; Play Length %s:%s (%s)" % (pl / 60, pl % 60, pl))

    @docopt_cmd
    def do_load(self, arg):
        """
Usage: load (db|mix <name>) <filepath> [--replace|--join]

Options:
    --replace           Replace existing entires with found entries
    --join              Only add new entries
        """
        global info_db, mix_db

        method = 'j' if arg['--join'] else 'r'
        file_path = arg['<filepath>']
        mix_name = ''

        if arg['db']:
            if not file_path.endswith('.db.json'):
                file_path = '{}.db.json'.format(file_path)

            if info_db['files'] and not arg['--replace'] and not arg['--join']:
                print("Info: Files already in structure!")
                return
        elif arg['mix']:
            if not file_path.endswith('.mix.json'):
                file_path = '{}.mix.json'.format(file_path)

            mix_name = arg['<name>']
            if mix_name in mix_db and not arg['--replace'] and not arg['--join']:
                print("Info: Mix already in structure!")
                return

            mix_db[mix_name] = []
        else:
            print('Info: Unknown load mode!')
            return

        if common.actions.file_exists(file_path, exception=False):
            if method == 'j':
                tmp_db = common.utils.read_json_data(file_path, byteify=False)
                if arg['db']:
                    info_db.update(tmp_db)
                elif arg['mix']:
                    mix_db[mix_name] = tmp_db['songs']
            elif method == 'r':
                tmp_db = common.utils.read_json_data(file_path, byteify=False)
                if arg['db']:
                    info_db = tmp_db
                elif arg['mix']:
                    mix_db[mix_name] = tmp_db['songs']
            else:
                print("Warning: Unknown method!")
        else:
            print("Info: File does not exists!")

        if arg['db']:
            Shell.calculate_stats(info_db)
            Shell.print_stats(info_db)
        elif arg['mix']:
            Shell.print_mix(mix_name, mix_db[mix_name])

    @docopt_cmd
    def do_locate(self, arg):
        """
Usage: locate <folder> [--filter=<filter>] [--replace|--join]

Options:
    --filter=<filter>   List of filters like: *.mp3,*.ogg
    --replace           Replace existing entires with found entries
    --join              Only add new entries

Description: Locates song files in a given folder and stores them in the current in memory database.
        """
        try:
            if info_db['files'] and not arg['--replace'] and not arg['--join']:
                print("Info: Files already in structure!")
                return

            method = 'j' if arg['--join'] else 'r'
            # ignore = os.path.expanduser('~')

            filters = arg['--filter']
            if filters is not None:
                filters = filters.split([',', ';'])
            else:
                filters = ['*.mp3']

            filename = ''
            try:
                files = info_db['files']
                for filename in common.locate(filters, root=arg['<folder>']):
                    fn = {'f': filename}
                    try:
                        _tmp = u'{}'.format((unicode(fn['f']).strip().upper()).encode('utf-8'))
                    except ValueError:
                        _tmp = fn['f'].strip().upper()

                    _md5 = hashlib.md5(_tmp).hexdigest()
                    if _md5 not in files:
                        files[_md5] = {
                            'filename': filename
                        }
                    elif method == 'r':
                        files[_md5]['filename'] = filename
                    else:
                        continue

                    folder = os.path.dirname(filename)
                    if folder not in info_db['folders']:
                        info_db['folders'].append(folder)

                        # f = {'f': filename}
                        # try:
                        #     l = [x for x in info_db['files'] if x.encode('utf-8') == f['f']]
                        # except ValueError:
                        #     l = [x for x in info_db['files'] if x == f['f']]
                        #
                        # if not l:
                        #     info_db['files'].append(filename)

            except ValueError as ve:
                Shell.add_problem_file('file path', filename)

                print("*** Error:", ve, "(", filename, ")")

            except Exception as e:
                Shell.add_problem_file('file', filename)

                print("*** Error:", e, "(", filename, ")")

        except KeyError as ke:
            print("Error: Unknown key (", ke, ")")

        except Exception as e:
            print("Error:", e)

        else:
            Shell.calculate_stats(info_db)
            Shell.print_stats(info_db, skip_zero=True)

    @docopt_cmd
    def do_analyse(self, arg):
        """
Usage: analyse

Description: Analyse songs in the current loaded database for meta information stored in the files.
        """
        try:
            if my_args.debug:
                view('>', '[Debug] {}'.format(arg))

            if not info_db['files']:
                print("Info: No Files in structure!")
                return

            artists = info_db['artists']
            album_artists = info_db['album artists']
            albums = info_db['albums']
            songs = info_db['songs']
            genres = info_db['genres']
            files = info_db['files']
            for file_md5 in files:
                info = files[file_md5]
                song_file = info.get('filename')
                if not isinstance(song_file, unicode):
                    song_file = song_file.decode("utf-8")

                if song_file is None:
                    continue

                if common.file_exists(song_file, exception=False):
                    if not eyed3.mp3.isMp3File(song_file):
                        try:
                            eyed3.load(song_file)
                        except Exception as e:
                            print("Warning: Song file is not a mp3 (", song_file, ")", e)
                            continue

                    if my_args.debug:
                        view('>', song_file)

                    audiofile = eyed3.load(song_file)
                    if audiofile.tag:
                        if audiofile.tag.artist is not None:
                            artist = unicode(audiofile.tag.artist)
                            if artist.upper().startswith('THE'):
                                artist = u'{}, The'.format(str(artist.upper().replace('THE', '', 1).strip()).title())
                        else:
                            Shell.add_problem_file('artist', song_file)
                            artist = u'Unknowns, The'

                        if audiofile.tag.title is not None:
                            title = audiofile.tag.title
                        else:
                            Shell.add_problem_file('title', song_file)
                            title = u'Song #{}'.format(get_song_number())

                        if audiofile.tag.album_artist is not None:
                            album_artist = audiofile.tag.album_artist
                        else:
                            Shell.add_problem_file('album artist', song_file)
                            album_artist = u'Various Artists U'

                        if audiofile.tag.album is not None:
                            album = audiofile.tag.album
                        else:
                            Shell.add_problem_file('album', song_file)
                            album = u'Chart Hits U'

                        year = str(audiofile.tag.best_release_date)
                        track = str(audiofile.tag.track_num)
                        disc = str(audiofile.tag.disc_num)
                        bpm = str(audiofile.tag.bpm)
                        if audiofile.tag.genre is not None:
                            genre = u'({}, {})'.format(str(audiofile.tag.genre.id), str(audiofile.tag.genre.name))
                        else:
                            Shell.add_problem_file('genre', song_file)
                            genre = u'(13, Pop)'

                        song_key = None
                        if audiofile.tag.frame_set is not None:
                            if 'TKEY' in audiofile.tag.frame_set:
                                song_key = str(audiofile.tag.frame_set['TKEY'][0].text)
                    else:
                        Shell.add_problem_file('file', song_file)
                        artist = u'Unknowns, The'
                        title = u'Song #{}'.format(get_song_number())
                        album_artist = u'Various Artists U'
                        album = u'Chart Hits U'
                        year = 1900
                        track = u'(None, None)'
                        disc = u'(None, None)'
                        bpm = u'0'
                        genre = u'(13, Pop)'
                        song_key = u''

                    if audiofile.info:
                        if audiofile.info.time_secs is not None:
                            length = audiofile.info.time_secs
                        else:
                            Shell.add_problem_file('length', song_file)
                            length = 0

                    else:
                        Shell.add_problem_file('length', song_file)
                        length = 0

                    Shell.set_info_element(artist, artists, info, 'artist', song_file)
                    Shell.set_info_element(title, songs, info, 'title', song_file)
                    Shell.set_info_element(album_artist, album_artists, info, 'album artist', song_file)
                    Shell.set_info_element(album, albums, info, 'album', song_file)
                    Shell.set_info_element(length, None, info, 'length', song_file)
                    Shell.set_info_element(year, None, info, 'year', song_file)
                    Shell.set_info_element(track, None, info, 'track', song_file)
                    Shell.set_info_element(disc, None, info, 'disc', song_file)
                    Shell.set_info_element(bpm, None, info, 'bpm', song_file)
                    Shell.set_info_element(song_key, None, info, 'key', song_file)
                    Shell.set_info_element(genre, genres, info, 'genre', song_file)

                else:
                    Shell.add_problem_file('file path', song_file)

                    print("Warning: Ignored file", song_file)

        except KeyError as ke:
            print("Error: Unknown key (", ke, ")")

        except Exception as e:
            print("Error:", e)

        else:
            Shell.calculate_stats(info_db)
            Shell.print_stats(info_db)

    @docopt_cmd
    def do_find(self, arg):
        """
Usage: find (song|artist|genre|album|albumartist|year) <string> [--max=<max>] [--file]
        """

        if arg and my_args.debug:
            print(arg)

        result = Shell.find_helper(arg)
        if result is None:
            return

        result = sorted(list(set(result)))
        if result:
            try:
                m = int(arg['--max'])
            except ValueError:
                m = 0
            except TypeError:
                m = 0

            if 0 < m < len(result):
                result = result[:m]

            print('\n'.join(result))
        else:
            print("Info: Nothing found!")

    @docopt_cmd
    def do_play(self, arg):
        """
Usage: play [(song|artist|album|genre|year|mix) <string>]
        """

        if arg and my_args.debug:
            print(arg)

        global pause_played
        if arg['mix']:
            name = arg['<string>']
            if name is None or not name:
                print("Info: Wrong or empty mix name!")
                return

            if name not in mix_db:
                print("Info: Unknown mix!")
                return

            result = mix_db[name]
            if result is None or not result:
                print("Info: Empty playlist!")
                return

        elif pause_played is not None:
            result = pause_played
            pause_played = None

        else:
            arg['--file'] = True
            result = Shell.find_helper(arg)
            if result is None or not result:
                print("Info: Empty playlist!")
                return

            result = sorted(list(set(result)))

        for item in result:
            q.put(item)

        if not threads:
            t = threading.Thread(target=worker)
            t.start()
            threads.append(t)

    @docopt_cmd
    def do_mix(self, arg):
        """
Usage: mix <name>
            [--method=(random|asc|desc)]
            [--length=<length> --unit=(min|max|title|artist|genre)]
            [--filter=<filters>]
            [--overwrite]

Options:
    --method=(random|asc|desc)              random=random picks, asc=fill from top, desc=fill from bottom
    --length=<length>                       length in units
    --unit=(min|max|title|artist|genre)     min=at least,max=at max,title=use titles,artists=use artists,genre=use genre
    --filter=<filters>                      filters combine with ";";
                                            bpm range from x to y -> bpm:x-y
                                            y bpm range around x -> bpm:x,y
                                            key k (e.g. 1A) to start with -> key:k
                                            text filter -> text:letter(s)|word:operator|text:letter1,letter2,
                                                eg. text:a,f or text:a-f or text:love,!
                                            operator ->
                                                = exact match, eg. text:adele,= -> adele is a word
                                                - do not include letter at start, eg. text:abc,-  -> abc are letters
                                                + include letter at start, eg. text:abc,+
                                                ? contains word, eg. text:love,? -> love is a word
                                                ! not contains word, wg. text:love,! -> love is a word
    --overwrite                             overwrite mix

Description: Creates a mix with a name and certain aspects
        """

        if arg and my_args.debug:
            print(arg)

        name = arg['<name>']
        if not name:
            print("Info: No mix name!")
            return

        if name not in mix_db or arg['--overwrite']:
            mix_db[name] = []
            method = Shell.option_helper(arg['--method'], ['random', 'asc', 'desc'], 'random')
            length = Shell.length_helper(arg['--length'])
            unit = Shell.option_helper(arg['--unit'], ['min', 'max', 'title', 'artist', 'genre'], 'min')
            unit = 'song' if 'title' == unit else unit
            song_filter, filters = Shell.song_filter_helper(arg['--filter'])

            saved = []
            for item in Shell.result_helper(method, length, unit, filters):
                if my_args.debug:
                    print("Info: Processing", item[0])

                mix_db[name].append(item[1])
                if item[0] not in saved:
                    saved.append(item[0])

            print("Info: Created mix '", name, "' with options '", method, length, unit, "' and filter '", song_filter, "'")
            Shell.print_mix(name, mix_db[name])

        else:
            print("Info: Found mix", name)

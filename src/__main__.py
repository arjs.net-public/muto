"""
Entry point for the main program.
"""
import common

# some constants
__author__ = 'alexrjs'

# some variables
specific_configuration = None


if __name__ == '__main__':
    common.shared.configuration = common.utils.fetch_configuration()
    args = common.utils.fetch_arguments(common.shared.configuration)
    common.messages.messages_extend(common.shared.configuration.items('messages'), args.debug)
    common.actions.check_options(args, common.shared.configuration)
    common.utils.run(args, common.shared.configuration)

# -*- coding: utf-8 -*-
import io
import os
import json
import yaml
import argparse
import fnmatch
import common.log
import common.shared
import common.messages
import commands
from configparser import ConfigParser
from docopt import docopt, DocoptExit

# some constants
__author__ = 'alexrjs'


def run(args, configuration):
    if args.verbose:
        common.log.view(
            '-',
            '{} - Version {}'.format(
                get_content(configuration.get('common', 'prg_name')).strip(),
                get_content(configuration.get('common', 'prg_version')).strip()
            )
        )

    pi_name = args.command
    plugin = getattr(commands, pi_name)
    try:
        # see if the plugin has a 'register' attribute
        plugin_action = plugin.run
    except AttributeError:
        # raise an exception, log a message,
        # or just ignore the problem
        exit(1)
    else:
        # try to call it, without catching any errors
        if args.verbose:
            common.log.view('-', plugin.version())

        exit(plugin_action(args))


def main(args):
    if args.debug or args.verbose:
        common.log.view('.' if args.verbose else '>', 'mode: {}'.format(args.actions))

    if not args.actions:
        raise RuntimeError(common.messages.MSG_E_0005)

    if common.shared.register[args.actions]:
        f = common.shared.register[args.actions]
        if not f:
            raise RuntimeError(common.messages.MSG_E_0007)

        f(args)
    else:
        raise RuntimeError(common.messages.MSG_E_0006)

    return 0


def get_content(filename):
    if filename.startswith('[') or filename.startswith('('):
        if os.path.exists(filename[1:-1]):
            return ''.join(read_text_data(filename[1:-1]))
        elif os.path.exists(os.path.basename(filename[1:-1])):
            return ''.join(read_text_data(os.path.basename(filename[1:-1])))

    return filename


def json_byteify(data):
    if isinstance(data, dict):
        return {json_byteify(key): json_byteify(value) for key, value in data.iteritems()}
    elif isinstance(data, list):
        return [json_byteify(element) for element in data]
    # elif isinstance(data, unicode):
    #    return data.encode('utf-8')
    else:
        return data


def read_json_data(filename, byteify=False):
    if not filename:
        raise AttributeError('{} ({})'.format(common.messages.MSG_E_0001, filename))

    if not os.path.exists(filename):
        raise IOError('{} ({})'.format(common.messages.MSG_E_0002, filename))

    if not os.path.isfile(filename):
        raise IOError('{} ({})'.format(common.messages.MSG_E_0003, filename))

    with open(filename) as json_file:
        json_data = json.load(json_file, encoding='UTF-8')

    if byteify:
        return json_byteify(json_data)
    else:
        return json_data


def write_json_data(filename, data, indent=4, sort=True, byteify=False, debug=False):
    if not filename:
        raise AttributeError('{} ({})'.format(common.messages.MSG_E_0001, filename))

    if debug:
        common.log.view('>', '[Debug] Filename: {}'.format(filename))
        common.log.view('>', '[Debug] Data: {}'.format(data))

    # if os.path.exists(filename):
    #     raise IOError('{} ({})'.format(shared.MSG_E_0008, filename))

    with open(filename, 'wb') as json_file:
        json.dump(
            data if not byteify else json_byteify(data),
            json_file,
            sort_keys=sort,
            indent=indent,
            encoding='UTF-8',
            separators=(', ', ' : ')
        )


def output_json(args, **kwargs):
    if args.debug:
        print('func: {}'.format(output_json.__name__))

    if 'name' not in kwargs or not kwargs['name']:
        raise AttributeError('{} - {}'.format(common.messages.MSG_E_0001, 'name='))

    if 'data' not in kwargs or not kwargs['data']:
        raise AttributeError('{} - {}'.format(common.messages.MSG_E_0001, 'data='))

    if 'subname' in kwargs and kwargs['subname']:
        _fn = os.path.join(args.output, '{}.{}.json'.format(kwargs['name'], kwargs['subname']))
    else:
        _fn = os.path.join(args.output, '{}.json'.format(kwargs['name']))

    if not _fn.endswith('.json'):
        _fn = '{}.json'.format(_fn)

    if args.debug:
        common.log.view('>', 'Filename used: {}'.format(_fn))

    with io.open(_fn, 'w', encoding='utf-8') as f:
        f.write(str(json.dumps(kwargs['data'], ensure_ascii=True, indent=4, sort_keys=True)))

    if args.debug:
        if 'title' in kwargs and kwargs['title']:
            common.log.view('>', 'Title: {}'.format(kwargs['title']))

        for k in kwargs['data']:
            common.log.view('>', '{} -> {}'.format(k, kwargs['data'][k]))

        common.log.view('>', 'total: {}'.format(len(kwargs['data'].keys())))


def read_text_data(filename):
    if not filename:
        raise AttributeError('{} ({})'.format(common.messages.MSG_E_0001, filename))

    if not os.path.exists(filename):
        raise IOError('{} ({})'.format(common.messages.MSG_E_0002, filename))

    if not os.path.isfile(filename):
        raise IOError('{} ({})'.format(common.messages.MSG_E_0003, filename))

    with open(filename) as text_file:
        text_data = text_file.readlines()

    return text_data


def write_text_data(filename, data, debug=False):
    if not filename:
        raise AttributeError('{} ({})'.format(common.messages.MSG_E_0001, filename))

    if debug:
        common.log.view('>', '[Debug] Filename: {}'.format(filename))
        common.log.view('>', '[Debug] Data: {}'.format(data))

    # if os.path.exists(filename):
    #     raise IOError('{} ({})'.format(shared.MSG_E_0008, filename))

    with open(filename, 'wb') as text_file:
        text_file.write(data)


def read_yaml_data(filename):
    if not filename:
        raise AttributeError('{} ({})'.format(common.messages.MSG_E_0001, filename))

    if not os.path.exists(filename):
        raise IOError('{} ({})'.format(common.messages.MSG_E_0002, filename))

    if not os.path.isfile(filename):
        raise IOError('{} ({})'.format(common.messages.MSG_E_0003, filename))

    with open(filename) as yaml_file:
        yaml_data = yaml.load(yaml_file)

    return yaml_data


def fetch_configuration(filename='default.config'):
    _filepath = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
    if filename and os.path.exists(filename):
        _config_file = filename
    elif _filepath and os.path.exists(_filepath):
        _config_file = os.path.join(_filepath, filename)

    if _config_file and not os.path.exists(_config_file):
        raise EnvironmentError('{} ({})'.format(common.messages.MSG_E_0002, filename))

    _configuration =  ConfigParser()
    _configuration.read(_config_file)

    return _configuration


def fetch_arguments(configuration, name=None, new_args=None):
    if not configuration:
        raise AttributeError('{} - {}'.format(common.messages.MSG_E_0001, 'configuration'))

    loaded_commands = 'loaded commands: %s' % '|'.join(commands.__all__)

    if not name:
        if configuration.has_option('common', 'prg_name'):
            name = get_content(configuration.get('common', 'prg_name')).strip().split()[0]

    if name:
        parser = argparse.ArgumentParser(prog=name, epilog=loaded_commands)
    else:
        parser = argparse.ArgumentParser(epilog=loaded_commands)

    added = []
    outs = []
    if not parser:
        raise SystemError('No parser!')

    for name, value in configuration.items('options'):
        if not value.lower() in ['true', 't', '1', 'yes', 'y']:
            outs.append('Ignored argument: {}'.format(name))
            continue

        _short = None
        _section = 'option:{}'.format(name)
        if configuration.has_option(_section, 'short'):
            _short = configuration.get(_section, 'short').strip()

        _long = None
        if configuration.has_option(_section, 'long'):
            _long = configuration.get(_section, 'long').strip()

        _type = None
        if configuration.has_option(_section, 'type'):
            _type = configuration.get(_section, 'type').strip().lower()

        if not _type:
            _type = 'param'

        _required = False
        if configuration.has_option(_section, 'required'):
            _required = configuration.getboolean(_section, 'required')

        _default = ''
        if configuration.has_option(_section, 'default'):
            _default = configuration.get(_section, 'default').strip()

        _description = 'Option: {}'.format(name)
        if configuration.has_option(_section, 'description'):
            _description = configuration.get(_section, 'description').strip()

        if name not in added:
            outs.append('Parsed argument: {}'.format(name))

        if _short and _long and _type:
            if 'bool' == _type:
                if _default:
                    _default = make_bool(_default)

                    parser.add_argument(
                        _short, _long, dest=name, action='store_true', required=_required,
                        default=_default, help=_description
                    )
                    outs.append('Added argument (1): {}'.format(name))
                else:
                    parser.add_argument(
                        _short, _long, dest=name, action='store_true', required=_required, help=_description
                    )
                    outs.append('Added argument (2): {}'.format(name))
            elif 'int' == _type:
                if _default:
                    parser.add_argument(
                        _short, _long, dest=name, type=int, required=_required, help=_description
                    )
                    outs.append('Added argument (3): {}'.format(name))
                else:
                    parser.add_argument(
                        _short, _long, dest=name, type=int, required=_required, default=_default, help=_description
                    )
                    outs.append('Added argument (4): {}'.format(name))
            elif 'param' == _type:
                if _default:
                    parser.add_argument(
                        _short, _long, dest=name, required=_required, default=_default, help=_description
                    )
                    outs.append('Added argument (5): {}'.format(name))
                else:
                    parser.add_argument(
                        _short, _long, dest=name, required=_required, help=_description
                    )
                    outs.append('Added argument (6): {}'.format(name))
        elif _short and _type:
            if 'bool' == _type:
                if _default:
                    _default = make_bool(_default)

                    parser.add_argument(
                        _short, dest=name, action='store_true', required=_required,
                        default=_default, help=_description
                    )
                    outs.append('Added argument (7): {}'.format(name))
                else:
                    parser.add_argument(
                        _short, dest=name, action='store_true', required=_required, help=_description
                    )
                    outs.append('Added argument (8): {}'.format(name))
            elif 'int' == _type:
                if _default:
                    parser.add_argument(
                        _short, dest=name, type=int, required=_required, default=_default, help=_description
                    )
                    outs.append('Added argument (9): {}'.format(name))
                else:
                    parser.add_argument(
                        _short, dest=name, type=int, required=_required, help=_description
                    )
                    outs.append('Added argument (10): {}'.format(name))
            elif 'param' == _type:
                if _default:
                    parser.add_argument(
                        _short, dest=name, required=_required, default=_default, help=_description
                    )
                    outs.append('Added argument (11): {}'.format(name))
                else:
                    parser.add_argument(
                        _short, dest=name, required=_required, help=_description
                    )
                    outs.append('Added argument (12): {}'.format(name))
        elif _long and _type:
            if 'bool' == _type:
                if _default:
                    _default = make_bool(_default)

                    parser.add_argument(
                        _long, dest=name, action='store_true', required=_required,
                        default=_default, help=_description
                    )
                    outs.append('Added argument (13): {}'.format(name))
                else:
                    parser.add_argument(
                        _long, dest=name, action='store_true', required=_required, help=_description
                    )
                    outs.append('Added argument (14): {}'.format(name))
            elif 'int' == _type:
                if _default:
                    parser.add_argument(
                        _long, dest=name, type=int, required=_required, default=_default, help=_description
                    )
                    outs.append('Added argument (15): {}'.format(name))
                else:
                    parser.add_argument(
                        _long, dest=name, type=int, required=_required, default=_default, help=_description
                    )
                    outs.append('Added argument (16): {}'.format(name))
            elif 'param' == _type:
                if _default:
                    # dest=name, required=_required,
                    parser.add_argument(
                        _long, default=_default, help=_description
                    )
                    outs.append('Added argument (17): {}'.format(name))
                else:
                    parser.add_argument(
                        _long, dest=name, required=_required, help=_description
                    )
                    outs.append('Added argument: (18) {}'.format(name))
        else:
            raise ValueError('{} - {} ({})'.format(common.messages.MSG_E_0001, 'name', str(name)))

    if new_args:
        args = parser.parse_args(args=new_args)
    else:
        args = parser.parse_args()

    if args.debug:
        for out in outs:
            common.log.view('.', out)

    return args


def make_bool(value):
    return not (value in ['F', 'f', 'N', 'n', 'No', 'no', 'False', 'false', 'FALSE'])


def dump_json_string(data, indent=4, sort=True, byteify=False, debug=False):
    if debug:
        common.log.view('>', 'Dump')

    return json.dumps(
        data if not byteify else json_byteify(data),
        sort_keys=sort,
        indent=indent,
        encoding='UTF-8',
        separators=(', ', ' : ')
    )


def pprint(data):
    """
    Pretty prints json data.
    :type data: dict
    """
    print(dump_json_string(data, byteify=True))


def locate(patterns, root=os.curdir, recursive=True, debug=False):
    folder = os.path.expanduser(root) if root.startswith('~') else root
    folder = os.path.abspath(folder)
    if debug:
        common.log.view('>', '[Debug] Folder: {}'.format(folder))

    if not os.path.exists(folder) or not os.path.isdir(folder):
        raise ValueError('{} ({})'.format(common.messages.MSG_E_0004, folder))

    if debug:
        common.log.view('>', '[Debug] patterns: {}'.format(patterns))

    for pattern in patterns:
        if debug:
            common.log.view('>', '[Debug] pattern: {}'.format(pattern))

        if recursive:
            for path, dirs, files in os.walk(folder, followlinks=True):
                for filename in fnmatch.filter(files, pattern):
                    yield os.path.join(path, filename)
        else:
            for filename in fnmatch.filter(os.listdir(folder), pattern):
                yield os.path.join(folder, filename)


def docopt_cmd(func):
    """
    This decorator is used to simplify the try/except block and pass the result
    of the docopt parsing to the called action.
    """
    def fn(self, arg):
        try:
            # print arg
            if '"' in arg:
                arg = arg.split('"')
                arg = [x.strip() for x in arg if x]
            elif "'" in arg:
                arg = arg.split("'")
                arg = [x.strip() for x in arg if x]

            # print arg
            opt = docopt(fn.__doc__, arg)

        except DocoptExit as e:
            # The DocoptExit is thrown when the args do not match.
            # We print a message to the user and the usage block.

            print('*** Error: Invalid Command! ({}{})'.format(
                fn.__name__.replace('do_', ''), ' {}'.format(arg) if arg else '')
            )
            print(e)
            return

        except SystemExit:
            # The SystemExit exception prints the usage for --help
            # We do not need to do the print here.

            return

        return func(self, opt)

    fn.__name__ = func.__name__
    fn.__doc__ = func.__doc__
    fn.__dict__.update(func.__dict__)
    return fn
